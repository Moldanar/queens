#ifndef CHESS_QUEENS_H
#define CHESS_QUEENS_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>

class chess_queens : public QWidget
{
    Q_OBJECT

public:
    chess_queens(QWidget *parent = nullptr);
private slots:
    void buttonClicked();
private:
    void newGame();
    void stepGame(int x, int y);
    void generateTable();
    void checkGame();

    int queen_num;
    int stepCount;
    QGridLayout* tableLayout;
    QVector<QVector<QPushButton*> > buttonTable;
    int** gameTable;
};

#endif // CHESS_QUEENS_H
