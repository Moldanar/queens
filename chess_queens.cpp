#include "chess_queens.h"
#include "ui_chess_queens.h"
#include <QMessageBox>

chess_queens::chess_queens(QWidget *parent) :
    QWidget(parent){
    setFixedSize(600,600);
    setWindowTitle(tr("Királynők"));

    tableLayout = new QGridLayout();
    generateTable();
    setLayout(tableLayout);

    QWidget::setStyleSheet("QPushButton:disabled { background-color:red } QPushButton:enabled { background-color:green }");

    newGame();
}

void chess_queens::newGame()
{
    for (int i = 0; i < 8; ++i)
        for (int j = 0; j < 8; ++j)
        {
            gameTable[i][j] = 0;
            buttonTable[i][j]->setStyleSheet("QPushButton:enabled{ background-color:green}");
            buttonTable[i][j]->setEnabled(true);
        }

    stepCount = 0;
    queen_num = 0;
}

void chess_queens::generateTable()
{
    gameTable = new int*[8];
    buttonTable.resize(8);

    for (int i = 0; i < 8; ++i)
    {
        gameTable[i] = new int[8];
        buttonTable[i].resize(8);
        for (int j = 0; j < 8; ++j)
        {
            buttonTable[i][j]= new QPushButton(this);
            buttonTable[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            tableLayout->addWidget(buttonTable[i][j], i, j);

            connect(buttonTable[i][j], SIGNAL(clicked()), this, SLOT(buttonClicked()));
        }
    }
}

void chess_queens::buttonClicked()
{
    QPushButton* senderButton = dynamic_cast <QPushButton*> (QObject::sender());
    int location = tableLayout->indexOf(senderButton);

    stepGame(location / 8, location % 8);
}

void chess_queens::stepGame(int x, int y)
{
    /*Ötlet
     *
     *A gameTable tárolja el, hogy hány királynő üti az adott mezőt
     *Ha az adott mezőn van a királynő akkor a szám legyen -1
     *Ha nem üti semmi sem a mezőt akkor értelemszerűen a gameTable 0-t tároljon
     */
    if(gameTable[x][y] == -1){
        gameTable[x][y] = 0;
        buttonTable[x][y]->setStyleSheet("QPushButton:enabled{background-color: green}");
        for(int i = 0; i < 8; ++i){
            if(i != y){
                if(--gameTable[x][i] == 0) buttonTable[x][i]->setEnabled(true);
            }

            if(i != x){
                if(--gameTable[i][y] == 0) buttonTable[i][y]->setEnabled(true);
            }
        }
        int lhs = (x < y) ? x : y;

        for(int i = 1; i <= lhs; ++i){
            if(--gameTable[x-i][y-i] == 0) buttonTable[x-i][y-i]->setEnabled(true);
        }

        int rhs = (x < y) ? (7-y) : (7-x);

        for(int i = 1; i <= rhs; ++i){
            if(--gameTable[x+i][y+i] == 0) buttonTable[x+i][y+i]->setEnabled(true);
        }

        lhs = (x < (7-y)) ? x : (7-y);

        for(int i = 1; i <= lhs; ++i){
            if(--gameTable[x-i][y+i] == 0) buttonTable[x-i][y+i]->setEnabled(true);
        }

        rhs = (y < (7-x)) ? y : (7-x);

        for(int i = 1; i<= rhs; ++i){
            if(--gameTable[x+i][y-i] == 0) buttonTable[x+i][y-i]->setEnabled(true);
        }

        queen_num--;
    }else if(gameTable[x][y] == 0){

        //oszlopok sorok kirajzolása

        for(int i = 0; i < 8; ++i){
            if(i != y){
                if(++gameTable[x][i] == 1) buttonTable[x][i]->setEnabled(false);
            }

            if(i != x){
                if(++gameTable[i][y] == 1) buttonTable[i][y]->setEnabled(false);
            }
        }

        // A "\" átló kirajzolása

        int lhs = (x < y) ? x : y;

        for(int i = 1; i <= lhs; ++i){
            if(++gameTable[x-i][y-i] == 1) buttonTable[x-i][y-i]->setEnabled(false);
        }

        int rhs = (x < y) ? (7-y) : (7-x);

        for(int i = 1; i <= rhs; ++i){
            if(++gameTable[x+i][y+i] == 1) buttonTable[x+i][y+i]->setEnabled(false);
        }

        // A "/" átló kirajzolása

        lhs = (x < (7-y)) ? x : (7-y);

        for(int i = 1; i <= lhs; ++i){
            if(++gameTable[x-i][y+i] == 1) buttonTable[x-i][y+i]->setEnabled(false);
        }

        rhs = (y < (7-x)) ? y : (7-x);

        for(int i = 1; i<= rhs; ++i){
            if(++gameTable[x+i][y-i] == 1) buttonTable[x+i][y-i]->setEnabled(false);
        }

        gameTable[x][y] = -1;
        buttonTable[x][y]->setStyleSheet("QPushButton{background-color: yellow}");

        queen_num++;
    }
    stepCount++;

    checkGame();
}

void chess_queens::checkGame()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Játék vége");
    msgBox.setText(QString::number(stepCount) + " lépésből nyerted meg a játékot");
    if(queen_num == 8){ msgBox.exec(); newGame();}
}

